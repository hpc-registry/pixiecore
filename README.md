# pixiecore

The purpose of this repository is to build an image with pixiecore binary that
allows to PXE boot hosts on the local network segment.

It works in tandem with a DHCP server which must be provided and configured separately.

Check out [pixiecore
documentation](https://github.com/danderson/netboot/tree/main/pixiecore#pixiecore)
to learn how it works.

## cmdline-builder

The [cmd](cmd/) (aka cmdline-builder) binary is supposed to build kernel
cmdline for PreOS and is meant to be run alongside [pixiecore in API
mode](https://github.com/danderson/netboot/tree/main/pixiecore#pixiecore-in-api-mode).

Q: Why is it needed?

A: Because PreOS initramfs scrips expect the correct `BOOTIF=01-${mac}`
   parameter to be passed to kernel. pixiecore's built-in templating capabilities
   do not suffice to template the `mac`. The purpose of cmdline-builder is to
   parse MAC address of the booting client and return it back in the cmdline line.

## Usage

Viz [hpc/charts/self-bootstrap](https://sissource.ethz.ch/hpc/charts/self-bootstrap).
