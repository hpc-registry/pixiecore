package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
)

/* Recognized Env variables:
 *  - LISTEN_ADDRESS
 *  - SSH_USER_CA
 *  - PREOS_DATA_ROOT_PATH  (e.g. /srv/pxe/preos)
 */

type PixieResponse struct {
	Kernel  string   `json:"kernel"`
	Initrd  []string `json:"initrd"`
	Cmdline string   `json:"cmdline"`
}

var (
	reBootPath *regexp.Regexp
)

const (
	defaultListenAddress = "127.0.0.1:20081"
	defaultPreOSDataRootPath = "/srv/pxe/preos"
)

func init() {
	reBootPath = regexp.MustCompile("^/?v1/boot/(?P<hwmac>[^/]+)$")
}

func mkCmdLine(hwmac, sshUserCA string) string {
	cmdline := fmt.Sprintf("BOOTIF=01-%s net.ifnames=0 biosdevname=0", hwmac)
	if len(sshUserCA) > 0 {
		cmdline += fmt.Sprintf(" preos.user_ca=\"%s\"", sshUserCA)
	}
	return cmdline
}

func getHWMac(r *http.Request) (string, error) {
	hwmac := reBootPath.ReplaceAllString(r.URL.Path, "${hwmac}")
	if hwmac == "" {
		return "", fmt.Errorf("failed to parse URL")
	}
	return hwmac, nil
}

func mkBootHandler() func(w http.ResponseWriter, r *http.Request) {
	sshUserCA := os.Getenv("SSH_USER_CA")
	preosDataRootPath := os.Getenv("PREOS_DATA_ROOT_PATH")
	if len(preosDataRootPath) == 0 {
		preosDataRootPath = defaultPreOSDataRootPath
	}

	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "GET":
			hwmac, err := getHWMac(r)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				fmt.Fprintf(os.Stderr, "%v\n", err)
				return
			}

			r := PixieResponse{
				Kernel:  "file://" + filepath.Join(preosDataRootPath, "kernel"),
				Initrd:  []string{"file://" + filepath.Join(preosDataRootPath, "initramfs"),},
				Cmdline: mkCmdLine(hwmac, sshUserCA),
			}
			j, _ := json.Marshal(r)
			fmt.Fprintf(os.Stderr, "%s\n", j)
			_, err = w.Write(j)
			if err != nil {
				fmt.Fprintf(os.Stderr, "failed to reply: %v\n", err)
			}
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	}
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func main() {
	http.HandleFunc("/v1/boot/", mkBootHandler())
	http.HandleFunc("/health", healthHandler)
	listenAddress := os.Getenv("LISTEN_ADDRESS")
	if len(listenAddress) == 0 {
		listenAddress = defaultListenAddress
	}

	fmt.Fprintf(os.Stderr, "listening on %s...\n", listenAddress)
	err := http.ListenAndServe(listenAddress, nil)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
}
